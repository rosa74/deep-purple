<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <?php wp_head(); ?>
</head>

<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-dark bg-primary">
            <a class="navbar-brand" href="#">Deep Purple</a>
            <div class="d-none d-md-block">
                <?php wp_nav_menu([
                    "theme_location" => "nav_menu",
                    "menu_class" => "navbar-nav"
                ]) ?>
            </div>
        </nav>
    </header>
    <div class="container mt-4">