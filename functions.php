<?php
if (!function_exists('registerMyPostTypes')) {
    function registerMyPostTypes()
    {
        $labelsMembre = array(
            "name"                  => __("membres", "enssop"),
            "singular_name"         => __("membres", "enssop"),
            "menu_name"             => __("membres", "enssop"),
            "all_items"             => __("Tout les chanteurs", "enssop"),
            "add_new"               => __("Ajouter un chanteur", "enssop"),
            "add_new_item"          => __("Ajouter un nouveau chanteur", "enssop"),
            "edit_item"             => __("Ajouter un chanteur", "enssop"),
            "new_item"              => __("Nouveau chanteur", "enssop"),
            "view_item"             => __("Voir le chanteur", "enssop"),
            "view_items"            => __("Voir les chanteur", "enssop"),
            "search_items"          => __("Chercher un chanteur", "enssop"),
            "not_found"             => __("Pas de chanteur trouvé", "enssop"),
            "not_found_in_trash"    => __("Pas de chanteur trouvé dans la corbeille", "enssop"),
            "featured_image"        => __("Image mise en avant pour ce chanteur", "enssop"),
            "set_featured_image"    => __("Définir l'image mise en avant pour ce chanteur", "enssop"),
            "remove_featured_image" => __("Supprimer l'image mise en avant pour ce chanteur", "enssop"),
            "use_featured_image"    => __("Utiliser comme image mise en avant pour ce chanteur", "enssop"),
            "archives"              => __("Type de chanteur", "enssop"),
            "insert_into_item"      => __("Ajouter au chanteur", "enssop"),
            "uploaded_to_this_item" => __("Ajouter au chanteur", "enssop"),
            "filter_items_list"     => __("Filtrer la liste de chanteur", "enssop"),
            "items_list_navigation" => __("Naviguer dans la liste de chanteur", "enssop"),
            "items_list"            => __("Liste de chanteur", "enssop"),
            "attributes"            => __("Paramètres du chanteur", "enssop"),
            "name_admin_bar"        => __("chanteur", "enssop"),
        );
        $argsMembre = array(
            "label"     => __('chanteur', 'enssop'),
            "labels"    => $labelsMembre,
            "description"   =>  __('chanteur du site', 'enssop'),
            "public"    => true,
            "publicly_queryable"    => true,
            "show_ui"   =>  true,
            "delete_with_user"  =>  false,
            "show_in_rest"  => false,
            "has_archive"   =>  true,
            "show_in_menu"  =>  true,
            "show_in_nav_menu"  => true,
            "menu_position"     =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array("slug" => "chanteur", "with_front" => true),
            "query_var"             => 'chanteur',
            "menu_icon"             => "dashicons-format-audio",
            "supports"              => array("title", 'editor', 'thumbnail'),
        );
        register_post_type("membre", $argsMembre);
        $labels1 = array(
            "name"                  => __("photo", "enssop"),
            "singular_name"         => __("photo", "enssop"),
            "menu_name"             => __("photo", "enssop"),
            "all_items"             => __("Tout les images", "enssop"),
            "add_new"               => __("Ajouter une image", "enssop"),
            "add_new_item"          => __("Ajouter une nouvelle image", "enssop"),
            "edit_item"             => __("Ajouter une image", "enssop"),
            "new_item"              => __("Nouvelle image", "enssop"),
            "view_item"             => __("Voir l image", "enssop"),
            "view_items"            => __("Voir les images", "enssop"),
            "search_items"          => __("Chercher une image", "enssop"),
            "not_found"             => __("Pas d image trouvé", "enssop"),
            "not_found_in_trash"    => __("Pas d image trouvé dans la corbeille", "enssop"),
            "featured_image"        => __("Image mise en avant pour cette image", "enssop"),
            "set_featured_image"    => __("Définir l'image mise en avant pour cette image", "enssop"),
            "remove_featured_image" => __("Supprimer l'image mise en avant pour cette image", "enssop"),
            "use_featured_image"    => __("Utiliser comme image mise en avant pour cette image", "enssop"),
            "archives"              => __("Type d image", "enssop"),
            "insert_into_item"      => __("Ajouter au image", "enssop"),
            "uploaded_to_this_item" => __("Ajouter au image", "enssop"),
            "filter_items_list"     => __("Filtrer la liste de image", "enssop"),
            "items_list_navigation" => __("Naviguer dans la liste de image", "enssop"),
            "items_list"            => __("Liste de image", "enssop"),
            "attributes"            => __("Paramètres du image", "enssop"),
            "name_admin_bar"        => __("images", "enssop"),
        );
        $args1 = array(
            "label"     => __('image', 'enssop'),
            "labels"    => $labels1,
            "description"   =>  __('image du site', 'enssop'),
            "public"    => true,
            "publicly_queryable"    => true,
            "show_ui"   =>  true,
            "delete_with_user"  =>  false,
            "show_in_rest"  => false,
            "has_archive"   =>  true,
            "show_in_menu"  =>  true,
            "show_in_nav_menu"  => true,
            "menu_position"     =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array("slug" => "gallery", "with_front" => true),
            "query_var"             => 'image',
            "menu_icon"             => "dashicons-images-alt2",
            "supports"              => array('thumbnail'),
        );
        register_post_type("photo", $args1);
    }
}
add_action("init", "registerMyPostTypes");
add_theme_support('post-thumbnails');



if (!function_exists("addStylesheets")) {
    function addStylesheets()
    {
        wp_enqueue_style("my-stylesheet", get_stylesheet_directory_uri() . '/style.css');
        wp_enqueue_style("font-awesome", "https://use.fontawesome.com/releases/v5.6.3/css/all.css");
        wp_enqueue_style("bootstrap", get_stylesheet_directory_uri() . "/css/bootstrap.min.css");
        wp_enqueue_script("jquery", "https://code.jquery.com/jquery-3.2.1.slim.min.js");
        wp_enqueue_script("bootstrap-js", get_stylesheet_directory_uri() . '/js/bootstrap.min.js');
    }
}
add_action( 'wp_enqueue_scripts', 'addstylesheets');

register_nav_menus([
    "nav_menu" => __('Menu principal', 'enssop'),
    "footer_menu" => __('Menu de pied de page', 'enssop')
]);
// register_nav_menu( "nav_menu", __('Menu principal', 'enssop') );
