<?php get_header(); ?>
<div class="jumbotron">
    <h1 class="text-center">Membres</h1>
</div>
<div class="row">

    <?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
    ?>
            <div class="mb-4 col-md-4">
                <div class="card">
                    <img src="<?php the_post_thumbnail_url() ?>" class="card-img-top" alt="">
                    <div class="card-body">
                        <p class="card-text"><?php echo get_the_title(); ?></p>
                        <div class="btn-group">
                            <a href="<?php the_permalink() ?>" class="btn btn-sm btn-outline-success">View</a>
                        </div>
                    </div>
                </div>
            </div>
    <?php
        }
    }
    ?>
</div>
<?php get_footer(); ?>