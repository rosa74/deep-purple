<?php get_header(); ?>

<?php if (have_posts()) :
        while (have_posts()) :
                the_post();
                echo '
    <div class="jumbotron">
        <h1 class="text-center">' . get_the_title() . '</h1>
    </div>
    <div class="row">
        <div class="col-md-6">
            <img src="' . get_the_post_thumbnail_url() . '" class="w-100" alt="">
        </div>
        <div class="col-md-6">' . get_the_content() . '</div>
    </div>
    ';
    ?>
 <div> date d'entrée dans le groupe : <?php  the_field("date_dentree")?></div>

<div> <img src=" <?php  the_field("photo") ?>" alt=""> </div>


        <?php endwhile;
endif; ?>
<?php get_footer(); ?>
